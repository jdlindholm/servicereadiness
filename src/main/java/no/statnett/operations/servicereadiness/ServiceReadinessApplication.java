package no.statnett.operations.servicereadiness;

import io.micronaut.runtime.Micronaut;

public class ServiceReadinessApplication {

    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }
}
