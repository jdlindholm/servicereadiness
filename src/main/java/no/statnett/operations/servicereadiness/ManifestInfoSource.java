package no.statnett.operations.servicereadiness;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;
import io.micronaut.context.env.MapPropertySource;
import io.micronaut.context.env.PropertySource;
import io.micronaut.core.async.SupplierUtil;
import io.micronaut.core.io.ResourceResolver;
import io.micronaut.management.endpoint.info.InfoEndpoint;
import io.micronaut.management.endpoint.info.InfoSource;
import io.micronaut.runtime.context.scope.Refreshable;
import io.reactivex.Flowable;
import org.reactivestreams.Publisher;

import java.io.InputStream;
import java.util.function.Supplier;

// Component is refreshable via a refresh event
@Refreshable
// Only create this component when the following
// requirements are met:
@Requires(beans = InfoEndpoint.class)
@Requires(property = "endpoints.info.config.enabled", notEquals = "false")
public class ManifestInfoSource implements InfoSource {
    private Manifest manifest;

    public ManifestInfoSource(ResourceResolver resourceResolver,
                              @Value("${endpoints.info.manifest:manifest.yml}")
                                 String manifestFile) {
        this.supplier = SupplierUtil.memoized(this::retrieveManifestInfo);

        System.out.println("manifest file: " + manifestFile);
        InputStream inputStream = this.getClass().getClassLoader()
                .getResourceAsStream(manifestFile);
        this.manifest = new ManifestReader().manifestReader(inputStream);
    }

    /**
     * Supplier for returning result.
     */
    private final Supplier<PropertySource> supplier;

    @Override
    public Publisher<PropertySource> getSource() {
        return Flowable.just(supplier.get());
    }

    private MapPropertySource retrieveManifestInfo() {
        return new MapPropertySource("manifest", manifest.manifestMap());
    }
}