package no.statnett.operations.servicereadiness;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;

@Controller("/services")
public class ServiceReadinessController {

    @Get("/")
    @Produces(MediaType.APPLICATION_JSON)
    public String services() {
        return "{'dummy', 'string'}";
    }
}