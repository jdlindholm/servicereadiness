package no.statnett.operations.servicereadiness

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import io.micronaut.scheduling.annotation.Scheduled
import org.apache.logging.log4j.LogManager
import javax.inject.Singleton


class ServiceProbe {
    companion object {
        private val logger = LogManager.getLogger()
    }

    fun probe(baseUrl: String, serviceResource: String): String? {

        val url = baseUrl + serviceResource
        logger.info("Probe url: {}", url)
        val (request, response, result) = Fuel.get(url)
                .responseString()

        val (payload, error) = result

        when (result) {
            is Result.Failure -> {
                val ex = result.getException()
            }
            is Result.Success -> {
                val data = result.get()
//                logger.debug("Result: {}", data)
                return data
            }
        }
        return ""
    }

}

@Singleton
class ServiceProbeJob(private var repository: ServiceInfoRepository) {

    companion object {
        private val logger = LogManager.getLogger()
    }

    @Scheduled(fixedDelay = "10m", initialDelay = "10s")
    fun executeFixedProbe() {
        logger.info("Schedueld manifest prober started")

        // 1. Get all services form the services data repository
        val services = repository.servicesCol()

        // 2. For every manifest call health, info


        // 3. Store the result in the manifest data repo.


    }
}