package no.statnett.operations.servicereadiness

import io.micronaut.context.annotation.Property
import io.micronaut.context.annotation.Value
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get

@Controller("/demo")
class DemoController(
        @Value("\${demo.name:default-name.yml}") val name: String,
        @Property(name = "demo.version") val version: String) {

    @Value("\${demo.other-property:other-default}")
    lateinit var otherProperty: String

    init {
        println("name:" + name)
    }

    @Get("/")
    fun index(): String {
        return "$name, $version, $otherProperty"
    }
}