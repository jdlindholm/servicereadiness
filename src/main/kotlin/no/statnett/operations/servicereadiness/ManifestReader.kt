package no.statnett.operations.servicereadiness

import org.yaml.snakeyaml.Yaml
import org.yaml.snakeyaml.constructor.Constructor
import org.yaml.snakeyaml.introspector.Property
import org.yaml.snakeyaml.introspector.PropertyUtils
import java.beans.IntrospectionException
import java.io.InputStream

data class FunctionDependencies(var services: List<ServiceId>? = null)

data class Application(var healthResource: String = "", var infoResource: String = "", var statusResource: String = "", var metricsResource: String = "", var shutdown: String = "")

data class Platform(var os: String = "", var runtime: String = "", var runtimeVersionMin: String = "", var runtimeVersionMax: String = "")

data class ServiceInfo(var name: String = "", var description: String = "", var documentation: String = "", var location: String = "")

data class ServiceId(var namespace: String = "", var id: String = "")

data class Interface(var url: String = "")

data class Dependencies(var consumes: List<Interface>? = null, var produces: List<Interface>? = null)

data class Manifest(var manifestVersion: String? = null, var serviceId: ServiceId = ServiceId(), var serviceInfo: ServiceInfo? = null, var platform: Platform? = null, var application: Application? = null, var dependencies: Dependencies? = null) {

    // TODO: add more fields to show in /info resource
    fun manifestMap(): Map<String, String?> {
        return mapOf(Pair("manifestVersion", manifestVersion))

    }
}

// Function manifest data classes
data class FunctionId(var namespace: String = "", var id: String = "")

data class FunctionInfo(var name: String = "", var description: String = "", var documentation: String = "", var location: String = "")

data class Environment(var name: String = "", var type: String = "", var AdGroup: String = "", var baseUrl: String = "", var splunkIndex: String =  "", var splunkDashboardUrl: String = "")

data class FunctionManifest(var functionManifestVersion: String? = null, var functionId: FunctionId = FunctionId(), var functionInfo: FunctionInfo? = null, var environments: List<Environment>? = null, var services: List<ServiceId>? = null)


fun String.toCamelCase(): String {
    val nameBuilder = StringBuilder()
    var capitalizeNextChar = false
    var first = true

    for (i in 0 until length) {
        val c = this[i]
        if (!Character.isLetterOrDigit(c)) {
            if (!first) {
                capitalizeNextChar = true
            }
        } else {
            nameBuilder.append(if (capitalizeNextChar)
                Character.toUpperCase(c)
            else
                Character.toLowerCase(c))
            capitalizeNextChar = false
            first = false
        }
    }
    return nameBuilder.toString()
}

class ManifestReader {
    val constructor = Constructor(Manifest::class.java)
    val yaml = Yaml(constructor)

    init {
        constructor.propertyUtils = object : PropertyUtils() {
            @Throws(IntrospectionException::class)
            override fun getProperty(type: Class<out Any>, name: String): Property {
                var name = name
                if ((name.indexOf('-') > -1) or (name.indexOf('_') > -1)) {
                    name = name.toCamelCase()
                }
                return super.getProperty(type, name)
            }
        }
    }

    fun manifestReader(stream: InputStream): Manifest {
        val manifest = yaml.loadAs(stream, Manifest::class.java)

        return manifest
    }

    fun functionManifestReader(stream: InputStream): FunctionManifest {
        val functionManifest = yaml.loadAs(stream, FunctionManifest::class.java)

        return functionManifest
    }
}