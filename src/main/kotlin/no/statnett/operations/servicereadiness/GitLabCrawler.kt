package no.statnett.operations.servicereadiness

import com.github.kittinunf.fuel.Fuel
import com.jayway.jsonpath.JsonPath
import io.micronaut.context.annotation.ConfigurationProperties
import io.micronaut.scheduling.annotation.Scheduled
import org.apache.logging.log4j.kotlin.Logging
import javax.inject.Singleton

data class GitConfiguration(var gitUrl: String = "none", var token: String = "none", val manifestResource: String = "none")

@Singleton
class GitLabCrawlerOrchestration(val config: GitConfiguration) {


    fun collectServiceManifest() {
        // 1. Get all gitlab projects
        // 2. For every project try to fetch the manifest file.
        // 3. Parse and store the result - manifest or the exception
    }

}

// TODO: How to initialize? Into the controller directly as constructor injection instead?
// https://blog.jdriven.com/2018/10/micronaut-mastery-configuration-of-a-kotlin-based-micronaut-application/
@ConfigurationProperties("gitlab")
class GitlabConfig {
    lateinit var projecturl: String
    lateinit var token: String
    lateinit var manifestResource: String
}


@Singleton
class GitLabCrawler(val config: GitConfiguration) : Logging {

//    @Inject
//    lateinit var con: GitlabConfig

    fun findProjects(): List<Int> {
        logger.debug("GitConfig: ${config}")

//        logger.debug("ConfigurationProperties: ${con.token}")

        val (request, response, result) = Fuel.get(config.gitUrl)
                .header("Private-Token" to config.token)
                .responseString()
        val json = result.get()
//        logger.debug("Json response: {}", json)

        // TODO: handle invalid json
        val ctx = JsonPath.parse(json)

        val projectIds = ctx.read<List<Int>>("$.[*]id")

        return projectIds
    }

    fun getManifestfile(projectId: Int): String {
        logger.debug("GitConfig: ${config}")

        val url = config.gitUrl + "/" + projectId + config.manifestResource
        logger.debug("ManifesUrl: ${url}")

        val (request, response, result) = Fuel.get(url)
                .header("Private-Token" to config.token)
                .responseString()

        return result.get();

        /*
        when (result) {
            is Result.Failure -> {
                val ex = result.getException()
                logger.error("Ups! No manifest file")
                println(ex)
            }
            is Result.Success -> {
                val manifest = result.get()
                logger.debug("Manifest: ${manifest}")
                return manifest
            }
        }
        return ""
        */
    }
}

@Singleton
class GitCrawlerJob(private var repository: ServiceInfoRepository) : Logging {

    @Scheduled(fixedDelay = "1h", initialDelay = "10s")
    fun executeFixedProbe() {
        logger.info("Scheduled git crawler started")

        // 1. Get all projects

        // 2. For every project try to fetch the manifest file.

        // 3. Parse and store the result - manifest or the exception

    }
}