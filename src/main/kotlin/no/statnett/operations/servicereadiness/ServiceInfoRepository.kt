package no.statnett.operations.servicereadiness

import javax.inject.Singleton

data class ProbeInfo(var health: String = "", var status: String = "", var info: String = "")

data class Service(var serviceId: ServiceId, var manifest: Manifest?, var probeInfo: ProbeInfo?)

/**
 * Storing manifest information. Both from the manifest and manifest probe result.
 */
@Singleton
class ServiceInfoRepository {

    val services: MutableMap<ServiceId, Service> = HashMap()


    fun storeManifest(manifest: Manifest) {
        val serviceId = manifest.serviceId
        val service = services.getOrDefault(serviceId, Service(serviceId, null, null))

        service.manifest = manifest

        services.put(serviceId, service)
    }

    fun servicesCol(): Collection<Service> {
        return services.values
    }

    fun manifest(serviceId: ServiceId): Manifest? {
        return services.get(serviceId)?.manifest
    }

    fun service(serviceId: ServiceId): Service? {
        return services.get(serviceId)
    }

    fun storeServiceProbe(serviceId: ServiceId, probeInfo: ProbeInfo) {
        val service = services.getOrDefault(serviceId, Service(serviceId, null, null))
        service.probeInfo = probeInfo
        services.put(serviceId, service)
    }

    fun clear() {
        services.clear()
    }

}

data class ProbeResult(var httpStatusCode: String = "", var body: String = "") {

}
