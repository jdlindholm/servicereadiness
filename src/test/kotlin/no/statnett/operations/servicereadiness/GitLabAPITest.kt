package no.statnett.operations.servicereadiness

import com.github.kittinunf.fuel.Fuel
import com.jayway.jsonpath.JsonPath
import org.apache.logging.log4j.kotlin.Logging
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

class GitLabAPITest : Logging {

    // Token for Gitlab
    val token = "<add token>"

    @Test
    @Disabled
    fun `project API test`() {
        val url = "https://gitlab.com/api/v4/projects"

        val (request, response, result) = Fuel.get(url)
                .header("Private-Token" to token)
                .responseString()
        val json = result.get()
        logger.debug("Json response: ${json}")

        val ctx = JsonPath.parse(json)

        val projectIds = ctx.read<List<Int>>("$..id")
        logger.debug("projectIds: ${projectIds}")


//        val projectName = ctx.read<String>("$.name[1]")
//        logger.debug("Project name: {}", projectName)
    }

}