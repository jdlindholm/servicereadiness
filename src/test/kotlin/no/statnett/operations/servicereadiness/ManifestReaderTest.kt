package no.statnett.operations.servicereadiness

import org.apache.logging.log4j.kotlin.Logging
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ManifestReaderTest : Logging {

    @Test
    fun `parse manifest file`() {
        val inputStream = this.javaClass
                .classLoader
                .getResourceAsStream("manifest_test1.yml")
        val manifest = ManifestReader().manifestReader(inputStream)
        logger.debug("Manifest: ${manifest}")

        assertEquals("1.0", manifest.manifestVersion)
        assertEquals("/metrics", manifest.application?.metricsResource)

        assertThat(manifest.platform?.runtime).isEqualTo("java")
    }
}