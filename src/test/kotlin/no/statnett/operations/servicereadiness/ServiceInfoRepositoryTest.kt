package no.statnett.operations.servicereadiness

import org.apache.logging.log4j.kotlin.Logging
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ServiceInfoRepositoryTest : Logging {

    lateinit var repo: ServiceInfoRepository

    @BeforeAll
    fun init() {
        repo = ServiceInfoRepository()
    }

    @BeforeEach
    fun reset() {
        repo.clear()
    }

    @Test
    fun `store manifest info`() {

        val manifest = loadManifest1()

        logger.debug("Manifest: ${manifest}")

        repo.storeManifest(manifest)

        val storedManifest = repo.manifest(ServiceId("no.statnett.operation", "test1"))

        logger.debug("Stored manifest: ${storedManifest}")

        Assertions.assertEquals("1.0", storedManifest?.manifestVersion)
        Assertions.assertEquals("/metrics", storedManifest?.application?.metricsResource)
    }


    @Test
    fun `get many manifest info`() {

        val manifest1 = loadManifest1()
        logger.debug("Manifest: ${manifest1}")
        repo.storeManifest(manifest1)

        val manifest2 = loadManifest2()
        logger.debug("Manifest: ${manifest2}")
        repo.storeManifest(manifest2)

        val services = repo.servicesCol()

        logger.debug("Services: ${services}")

        assertEquals(2, services.count())
    }

    @Test
    fun `store service probe result`() {
        val manifest1 = loadManifest1()
        repo.storeManifest(manifest1)

        val manifest2 = loadManifest2()
        repo.storeManifest(manifest2)

        repo.storeServiceProbe(manifest2.serviceId, ProbeInfo("OK", "Up", "{}"))

        val service = repo.service(manifest2.serviceId)

        println("service?.probeInfo?.status:" + service?.probeInfo?.status)

        assertThat(service?.probeInfo?.status).isEqualTo("Up")
    }

    fun loadManifest1(): Manifest {
        val inputStream1 = this.javaClass
                .classLoader
                .getResourceAsStream("manifest_test1.yml")
        val manifest1 = ManifestReader().manifestReader(inputStream1)
        return manifest1
    }

    fun loadManifest2(): Manifest {
        val inputStream2 = this.javaClass
                .classLoader
                .getResourceAsStream("manifest_test2.yml")
        val manifest2 = ManifestReader().manifestReader(inputStream2)
        return manifest2
    }
}