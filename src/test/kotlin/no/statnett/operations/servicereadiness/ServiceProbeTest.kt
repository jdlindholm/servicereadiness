package no.statnett.operations.servicereadiness

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.result.Result
import org.apache.logging.log4j.kotlin.Logging
import org.junit.jupiter.api.Test

class ServiceProbeTest : Logging {

    @Test
    fun `service probe test`() {

        val repo = ServiceInfoRepository()
        val manifest1 = loadManifest1()
        repo.storeManifest(manifest1)

        // get service
        val serviceId = ServiceId("no.statnett.operation", "test1")
        val service = repo.service(serviceId)

        val statusResource = service?.manifest?.application?.healthResource
        val url = "http://localhost:8080" + statusResource

        // probe
        logger.info("Probe url: ${url}")
        val (request, response, result) = Fuel.get(url)
                .responseString()

        val (payload, error) = result

        var status: String;

        when (result) {
            is Result.Failure -> {
                status = result.toString() //.getException().toString()
                logger.warn("Error in service: ${serviceId}. Exception: ${status}")
            }
            is Result.Success -> {
                status = result.get()
                logger.debug("Result: ${status}")
            }
        }

        // store result
        repo.storeServiceProbe(serviceId, ProbeInfo("", status, ""))

    }

    fun loadManifest1(): Manifest {
        val inputStream1 = this.javaClass
                .classLoader
                .getResourceAsStream("manifest_test1.yml")
        val manifest1 = ManifestReader().manifestReader(inputStream1)
        return manifest1
    }
}