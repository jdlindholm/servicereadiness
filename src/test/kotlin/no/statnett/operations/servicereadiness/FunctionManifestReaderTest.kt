package no.statnett.operations.servicereadiness

import org.apache.logging.log4j.kotlin.Logging
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class FunctionManifestReaderTest : Logging {

    @Test
    fun `parse function manifest file`() {
        val inputStream = this.javaClass
                .classLoader
                .getResourceAsStream("function-manifest_test.yml")
        val manifest = ManifestReader().functionManifestReader(inputStream)
        logger.debug("Manifest: ${manifest}")

        assertEquals("1.0", manifest.functionManifestVersion)
        assertEquals("odin", manifest.functionId.id)

        assertThat(manifest.services).contains(ServiceId("no.statnett.operation", "service-probe"))
    }
}