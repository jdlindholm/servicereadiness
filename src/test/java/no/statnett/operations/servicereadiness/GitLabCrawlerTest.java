package no.statnett.operations.servicereadiness;

import io.micronaut.context.annotation.Value;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

// TODO: integration test at a given site

@MicronautTest
public class GitLabCrawlerTest {

    @Value("https://${gitlab.projecturl}")
    String gitUrl;

    @Value("${gitlab.token}")
    String token;

//    @Inject
//    GitLabCrawler crawler;

    // remember to set the token in env.var before running the test
    @Test
    @Disabled
    public void find_project_ids() {
        System.out.println("Start!!" + gitUrl + " token: " + token);
        GitConfiguration config = new GitConfiguration(gitUrl, token, "");

        GitLabCrawler crawler = new GitLabCrawler(config);

        List<Integer> projectIds = crawler.findProjects();

        System.out.println("Project ids: " + projectIds);
//            logger.debug("Project ids: {}", projectIds)

        assertTrue(projectIds.contains(838));
    }
}
