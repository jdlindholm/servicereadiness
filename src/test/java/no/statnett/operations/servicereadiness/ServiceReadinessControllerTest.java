package no.statnett.operations.servicereadiness;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;

@MicronautTest
public class ServiceReadinessControllerTest {

    @Inject
    @Client("/")
    RxHttpClient client;

    @Test
    public void testServices() {

        final String result = client.toBlocking().retrieve(HttpRequest.GET("/services"));

        assertEquals("{'dummy', 'string'}", result);
    }
}
