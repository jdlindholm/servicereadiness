package no.statnett.operations.servicereadiness;

import com.github.tomakehurst.wiremock.WireMockServer;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.jupiter.api.Assertions.assertTrue;

// Mocking GitLab
public class GitLabCrawlerCollaboratorTest {

    private Logger logger = LogManager.getLogger();
    private static WireMockServer wireMockServer;

    private static GitConfiguration config = new GitConfiguration("http://localhost:8089/api/v4/projects", "my-token", "/repository/files/manifest.yml/raw?ref=master");
    private static GitLabCrawler crawler;

    @BeforeAll
    public static void init() {
        wireMockServer = new WireMockServer(options().port(8089));
        wireMockServer.start();
        crawler = new GitLabCrawler(config);
    }

    @AfterAll
    public static void close() {
        wireMockServer.stop();

    }

    @Inject
    @Client("/")
    RxHttpClient client;

    @Test
    public void testGitLabCrawlerProjects() {

        wireMockServer.stubFor(get("/api/v4/projects")
                .willReturn(aResponse().withStatus(200).withBodyFile("projects.json")));

        List<Integer> projectsIds = crawler.findProjects();

        logger.debug("projectsIds:" + projectsIds);
        assertTrue(projectsIds.contains(829), "Should contain project 829");
    }

    @Test
    public void testGitLabManifestAPI() {

        configureFor(8089);
        stubFor(get(urlEqualTo("/api/v4/projects/829/repository/files/manifest.yml/raw?ref=master"))
                .willReturn(aResponse().withStatus(200).withBodyFile("manifest.yml")));

        String manifestFileJson = crawler.getManifestfile(829);

        logger.debug("manifestFileJson:" + manifestFileJson);
//        assertTrue(projectsIds.contains(829), "Should contain project 829");

        verify(moreThanOrExactly(1), getRequestedFor(urlEqualTo("/api/v4/projects/829/repository/files/manifest.yml/raw?ref=master")));

    }
}
